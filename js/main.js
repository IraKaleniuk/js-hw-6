"use strict";

function createNewUser() {
    let fName = prompt("Enter your first name:");
    let lName = prompt("Enter your last name:");
    let birthday = prompt("Enter your birthday (dd.mm.yyyy)")

    birthday = new Date(Date.parse(birthday.split(".").reverse().join("-")));

    while (!fName || !lName || fName === "null" || lName === "null") {
        fName = prompt("Enter your first name:", fName);
        lName = prompt("Enter your last name:", lName);
    }
    const newUser = {
        firstName: fName,
        lastName: lName,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        setFirstName(newFirstName) {
            if (newFirstName) {
                Object.defineProperty(this, "firstName", {value: newFirstName});
            }
        },
        setLastName(newLastName) {
            if (newLastName) {
                Object.defineProperty(this, "lastName", {value: newLastName});
            }
        },
        getAge() {
            const now = new Date();
            let age = now.getFullYear() - birthday.getFullYear() - 1;
            if (now.getMonth() > birthday.getMonth() ||
                now.getMonth() === birthday.getMonth() && now.getDate() >= birthday.getDate()) {
                age += 1;
            }
            return age;
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthday.getFullYear();
        }
    }

    Object.defineProperties(newUser, {
        "firstName": {
            writable: false,
        },
        "lastName": {
            writable: false,
        }
    })

    return newUser;
}

const user1 = createNewUser();
console.log("New user: ")
console.log(user1);

const user1Pass = user1.getPassword();
console.log("User password: " + user1Pass);

const user1Age = user1.getAge();
console.log("User age: " + user1Age);



